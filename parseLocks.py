import json
import urllib.request
from urllib.error import HTTPError
from warnings import warn
import time
import threading
import concurrent.futures

from util import makeKmlGeo,makeColorIconStyles,makeOperatingTable
from util import scrapePhoneNumbers

from util import geofilterNL as geofilter


# cap the amount of locks to make testing faster. zero or negative means don't cap
debugcap = -1
#debugcap = 200

# filter on location. For instance only use netherlands.
def namefilter(name):
#    return name.lower().find('erasmus') >= 0
    return True
        

def makeLockPlacemark(lock):
    name = lock['name']
    lon = lock['geo']['coord'][0]['lon']
    lat = lock['geo']['coord'][0]['lat']
    if not geofilter(lon,lat):
        return ""
    if not namefilter(name):
        return ""
    url='https://vaarweginformatie.nl/frp/main/#/geo/detail/LOCK/%i' % lock['ref']
    
    print("{0}\n\t{1}".format(name, url)) 
    
    for retry in range(6):
        try:
            with urllib.request.urlopen('https://vaarweginformatie.nl/frp/api/geo/lock/%i'%lock['ref']) as extradatafile:
                extradata = json.load(extradatafile)
                
        except HTTPError as e:
                print( "Http error", retry, type(e), repr(e), e)
                # it is not our connection, since we got a response
                # retrying only would give us the same response
                extradata = lock # just juse what we have
                break
        except Exception as e:
            print( "fetch failed. Waiting and retrying.", retry, type(e), repr(e), e)
            time.sleep(5)
            continue
        break
        
    desc = '<a href="{url}" > source </a> <br />'.format(url=url) 
    try:
        #get vhf call points
        # probably a bug in the api. for bridges they use 'radioCallInPoints' (plural)
        for vhf in extradata['radioCallInPoint']:
            channel = ' , '.join(vhf['vhfChannels'])
            vname =vhf['name']
            desc += '<h2> VHF {channel}</h2>  {name}'.format(channel=channel, name=vname)
    except (KeyError,IndexError) as e:
        #print("failed to load vhf channels for ", lock['ref'], repr(e))
        pass
    
    
    xmlphones=""
    try:
        phones = scrapePhoneNumbers(extradata['phoneNumber'])
        desc += '\n<h2>phone</h2>\n' + extradata['phoneNumber']
        desc += "\n<ul>"
        for ph in phones:
            desc += '\n<li><a href="tel:{phone}">{phone}</a></li>,'.format( phone=ph) 
            xmlphones += '\n  <phoneNumber>%s</phoneNumber>\n'% ph
        desc += "\n</ul>"
        
    except KeyError:
        pass
    
    if 'operatingTimesId' in extradata:
        operatingId = extradata['operatingTimesId']
        desc += makeOperatingTable(operatingId)
    
    return """
    <Placemark>
    <styleUrl>#placemark-orange</styleUrl>  
        {xmlphones}
        <name><![CDATA[{name}]]></name>
        <description><![CDATA[{desc}]]></description>
        {geo}
    </Placemark>
    """.format(
        **locals(),
        geo=makeKmlGeo( lock['geo'] )
        )
    
    
if __name__ == "__main__":
    with urllib.request.urlopen("https://vaarweginformatie.nl/frp/api/geo/all?types=LOCK" ) as jsonfile: 
        locks = json.load(jsonfile)
        
    if 'debugcap' in locals() and debugcap > 0:
        locks = locks[:debugcap]
        
    kmlfile = open('locks.kml', 'tw') 
    kmlfile.write(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <kml xmlns="http://www.opengis.net/kml/2.2"> <Document> 
    """.strip()
        )
    kmlfile.write(makeColorIconStyles(['orange']) )
    kmlfile.write('<name>locks</name>')
    try:
        # we will fetch 20 locks symultaneously, but still write them sequentially using the futureIterator 
        with concurrent.futures.ThreadPoolExecutor(max_workers=30) as executor: 
            print ("starting to parse {n} locks".format(n=len(locks)) )
            res = executor.map(makeLockPlacemark, locks)
            kmlfile.writelines(res)
    finally:
        # if there is an exception, it will be uncaught, so printed, but we still neatly close our file.
        kmlfile.write( '</Document> </kml>\n ' )
        kmlfile.close()

