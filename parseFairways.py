import json
import urllib.request
from warnings import warn
import time
import re
import threading
import concurrent.futures

from util import makeKmlGeo

# thresholds for colours
mastDown = 1.5
mastUp   = 8

# cap the amount of bridges to make testing faster. negative means don't cap
debugcap = -1
#debugcap = 200

# filter on location. For instance only use netherlands.
# yes, we are excluding limbrug from the netherlands as it should be
def geofilter(lon,lat):
    return (
        #3.5     < lon < 7.2 and
        3.5     < lon < 5.2 and # west of nl only
        51.25 < lat < 53.4
    )

def namefilter(name):
#    return name.lower().find('erasmus') >= 0
    return True
        

    
    
def makeFairwayPlacemark(baseJson):
    ref = baseJson['ref']
    name = baseJson['name']
    
    ## no extra information in here
    #with urllib.request.urlopen("https://vaarweginformatie.nl/frp/api/geo/fairway/%i" % ref ) as jsonfile: 
    #    jfairway = json.load(jsonfile)
    
    
    ## these don't really map
    #with urllib.request.urlopen( 'https://vaarweginformatie.nl/frp/api/geo/maxdim/%i' % ref) as jsonfile: 
    #    jmaxdim = json.load(jsonfile)
    
    
    print(ref, name)
    # dumb bullet point info fields
    bullets = {}
    #bullets['routename'] = jfairway['route']['name']
    
    for name,value in bullets.items():
        print(" %s : %s " % (name,value) )
    
    geo = makeKmlGeo(baseJson['geo'])
        
    return """
    <Placemark>
    <name>{name}</name>
    {geo}
    </Placemark>
    """.format(**locals())
        
# if this file is run as main file, we make fairways.kml on our own
if __name__ == "__main__":
    
    with urllib.request.urlopen("https://vaarweginformatie.nl/frp/api/geo/all?types=FAIRWAY" ) as jsonfile: 
        fairways = json.load(jsonfile)
    kmlfile = open('fairways.kml', 'tw') 
        
    kmlfile.write(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <kml xmlns="http://www.opengis.net/kml/2.2"> <Document> 
    """.strip()
        )
    
    kmlfile.write('<name>fairways/name>')
    try:
        with concurrent.futures.ThreadPoolExecutor() as executor: 
            res = executor.map(makeFairwayPlacemark, fairways)
            kmlfile.writelines(res)
    finally:
        # if there is an exception, it will be uncaught, so printed, but we still neatly close our file.
        kmlfile.write( '</Document> </kml>\n ' )
        kmlfile.close()
