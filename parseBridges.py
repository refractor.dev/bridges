import json
import urllib.request
from warnings import warn
import time
import re
import threading
import concurrent.futures
from collections import defaultdict

from util import makeKmlGeo,makeColorIconStyles,makeOperatingTable
from util import scrapePhoneNumbers

from util import geofilterNL as geofilter


# thresholds for colours
mastDown = 1.5
mastUp   = 8

# cap the amount of bridges to make testing faster. zero or negative means don't cap
debugcap = -1
#debugcap = 200

# filter on location. For instance only use netherlands.
# yes, we are excluding limbrug from the netherlands as it should be
def namefilter(name):
#    return name.lower().find('erasmus') >= 0
    return True
        
openingNames = defaultdict(lambda x: f"{x!r}?", {
    "VST": "fixed",
    "KLP": "bascule",
    "BC" : "bascule",
    "DBC": "double bascule",
    "RBC": "rolling bascule bridge",
    "OPH": "drawbridge",
    "DOP": "double drawbridge",
    "DR" : "swing bridge",
    "DDR" : "double swing bridge",
    "ROL": "roling bridge",
    "HEF": "table bridge",
    "PON": "floating bridge",
    "PDR": "floating swing bridge",
    "OKW": "OKW?",
})


def makeBridgePlacemark(bridge):
    name = bridge['name']
    lon = bridge['geo']['coord'][0]['lon']
    lat = bridge['geo']['coord'][0]['lat']
    if not geofilter(lon,lat):
        return ""
    if not namefilter(name):
        return ""
    url='https://vaarweginformatie.nl/frp/main/#/geo/detail/BRIDGE/%i' % bridge['ref']
    
    print("{0}\n\t{1}".format(name, url)) 
    
    try:
        for retry in range(6):
            try:
                with urllib.request.urlopen('https://vaarweginformatie.nl/frp/api/geo/bridge/%i'%bridge['ref']) as extradatafile:
                    extradata = json.load(extradatafile)
            except Exception as e:
                print( "fetch failed. Waiting and retrying.", retry, type(e), repr(e), e)
                time.sleep(5)
                continue
            break
            
        desc = '<a href="{url}" > source </a> <br />'.format(url=url) 
        
        # whether one or more openings are movable (can open)
        movable = False
        highestOpening = -1
        
        try:
            extradata['openings'].sort(reverse=True, key = lambda o:o.get('heightClosed', 0) )  # sort by height. 
            desc += '<h2> openings </h2>'
            desc += 'reference: ' + extradata.get('referenceLevel', '-')
            
            for opening in extradata['openings'] :
                opening.setdefault('note', '-')
                opening.setdefault('heightClosed', 0)
                opening.setdefault('heightOpened', 0)
                opening.setdefault('width', 0)
                desc += """\n<h3> {typeName} opening </h3>
                    height closed:{heightClosed} <br>
                    width: {width} <br>
                    note: {note} <br>
                    height opened: {heightOpened} """ .format (**opening, typeName=openingNames[opening['type']] )
                    
                if opening['type'] != "VST" and not( opening['type'] == 'HEF' and opening['heightOpened'] < mastDown ) :
                    movable = True
                
            # we sorted them, so first is highest
            highestOpening = float(extradata['openings'][0]['heightClosed'])
            
        # either one of the fields is not provided, or we have 0 openings
        # don't care and continue
            pass
        except IndexError:
            pass
        
        bb = "BB " if movable else ""
        try:
            #get vhf call points
            for vhf in extradata['radioCallInPoints']:
                channel = ' , '.join(vhf['vhfChannels'])
                vname =vhf['name']
                desc += '<h3> VHF {channel}</h3>  {name}'.format(channel=channel, name=vname)
        except (KeyError,IndexError):
            pass
        
        
        
        if highestOpening > mastUp:
            color = 'green'
        elif highestOpening >= mastDown:
            color = 'yellow'
        elif movable:
            color = 'orange'
        else:
            color = 'red'
        
        xmlphones=""
        try:
            phones = scrapePhoneNumbers(extradata['phoneNumber'])
            desc += '\n<h2>phone</h2>\n' + extradata['phoneNumber']
            desc += "\n<ul>"
            for ph in phones:
                desc += '\n<li><a href="tel:{phone}">{phone}</a></li>,'.format( phone=ph) 
                xmlphones += '\n  <phoneNumber>%s</phoneNumber>\n'% ph
            desc += "\n</ul>"
            
        except KeyError:
            pass
        
        if 'operatingTimesId' in extradata:
            operatingId = extradata['operatingTimesId']
            desc += makeOperatingTable(operatingId)
        
        return """
        <Placemark>
        <styleUrl>#placemark-{color}</styleUrl>  
            {xmlphones}
            <name><![CDATA[{bb}{name} - {highestOpening:.1f}]]></name>
            <description><![CDATA[{desc}]]></description>
            {geo}
        </Placemark>
        """.format(
            **locals(),
            geo=makeKmlGeo( bridge['geo'] )
            )
        
    except Exception as e: 
        raise e
            
    
if __name__ == "__main__":
    with urllib.request.urlopen("https://vaarweginformatie.nl/frp/api/geo/all?types=BRIDGE" ) as jsonfile: 
        bridges = json.load(jsonfile)
        
    if debugcap > 0:
        bridges = bridges[:debugcap]
        
    kmlfile = open('bridges.kml', 'tw') 
    kmlfile.write(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <kml xmlns="http://www.opengis.net/kml/2.2"> <Document> 
    """.strip()
        )
    kmlfile.write(makeColorIconStyles(['red', 'green', 'orange' , 'yellow']) )
    kmlfile.write('<name>bridges</name>')
    try:
        # we will fetch 20 bridges symultaneously, but still write them sequentially using the futureIterator 
        with concurrent.futures.ThreadPoolExecutor(max_workers=30) as executor: 
            print ("starting to parse {n} bridges".format(n=len(bridges)) )
            res = executor.map(makeBridgePlacemark, bridges)
            kmlfile.writelines(res)
    finally:
        # if there is an exception, it will be uncaught, so printed, but we still neatly close our file.
        kmlfile.write( '</Document> </kml>\n ' )
        kmlfile.close()

