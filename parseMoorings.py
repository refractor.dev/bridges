import json
import urllib.request
from urllib.error import URLError
from warnings import warn
import time
import re
import threading
import concurrent.futures

from util import makeKmlGeo, makeColorIconStyles

from util import geofilterNL as geofilter

categories = {}


#debugcap = 200

def makeMooringPlacemark(basejson):
    name = basejson['name']
    ref = basejson['ref']
    
    if not geofilter(**basejson['geo']['coord'][0]):
        return ""
    try:
        category = basejson['metadata']['category']
    except KeyError:
        category = 'OTHER'
    
    # others are: 'LOADING_AND_UNLOADING', 'PUSH_TOW_VESSELS', 'NON_PUSH_TOW_VESSELS'
    # those are probably for commercial only
    if category not in ['OTHER', 'ALL_VESSELS']:
        return ""
    
    url = "https://vaarweginformatie.nl/frp/main/#/geo/detail/shipstation/%i" % ref
    i = 5
    while i > 0:
        try:
            with urllib.request.urlopen("https://vaarweginformatie.nl/frp/api/geo/shipstation/%i" %ref ) as jsonfile: 
                berth = json.load(jsonfile)
            break
        except urllib.error.URLError as error:
            print("error ", error)
            time.sleep(1)
            i-=1
    if i == 0:
        print ("warning: fetch failed for ", ref)
        return ""
        
    desc = '<a href="{url}"> source </a>' .format(url=url)
    res = ""
    print(name, url)
    for area in berth.get('waitingAreas', []):
        limRemark = area.get('limitationRemark', '-')
        try:
            adminType = area['administration']['type']
        except:
            adminType = ''
            
        if adminType in ["PRIV", "PART"]:
            continue
        res += """
        <Placemark>
        <styleUrl>#placemark-blue</styleUrl>  
            <name>
            <![CDATA[{name}]]>
            </name>
            <description>
            <![CDATA[
            {limRemark} <br>
            administration:{adminType}
            <hr>
            {desc}
            ]]>
            </description>
            {geo}
        </Placemark>\n""".format(
            **locals(),
            geo=makeKmlGeo( area.get('geo', basejson['geo'] ) )
            )
        
    return res


if __name__ == "__main__":
    with urllib.request.urlopen("https://vaarweginformatie.nl/frp/api/geo/all?types=BERTH" ) as jsonfile: 
        berths = json.load(jsonfile)
        
    if 'debugcap' in locals():
        warn("capping result count")
        berths = berths[:debugcap]
        
    kmlfile = open('moorings.kml', 'tw') 
    kmlfile.write(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <kml xmlns="http://www.opengis.net/kml/2.2"> <Document> 
    """.strip()
        )
    kmlfile.write(makeColorIconStyles('blue'))
    kmlfile.write('<name>moorings</name>')
    try:
        with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor: 
            print ("starting to parse {n} berths".format(n=len(berths)) )
            res = executor.map(makeMooringPlacemark, berths)
            kmlfile.writelines(res)
    finally:
        # if there is an exception, it will be uncaught, so printed, but we still neatly close our file.
        kmlfile.write( '</Document> </kml>\n ' )
        kmlfile.close()
