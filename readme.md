
# netherlands bridges and locks in kml

Make a series of kmls of bridges and locks by scraping vaarweginformatie.nl

These kmls can be loaded into most maps and navigation apps. 

The bridge have names like on common maps: with hights in meters and whether they are openable (BB).

# downloading kml's

This repository has a pipeline which runs weekly. see the [jobs](https://gitlab.com/refractor.dev/bridges/-/jobs) section, and download the latest.


