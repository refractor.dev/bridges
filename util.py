from datetime import datetime,timedelta
import json
import time
import urllib.request
import re

def geofilterNL(lon,lat):
    return (
        3.5     < lon < 7.2 and
        #3.5     < lon < 5.2 and # west of nl only
        51.25 < lat < 53.4
    )


def scrapePhoneNumbers(text):
        phone = (
            text
            .replace('(','')
            .replace(')','')
            .replace(' ','')
            )
        return re.findall('[0-9-]+', phone)
    


def makeKmlGeo(jsongeo):
    coordlist = '\n'.join([  '{lon},{lat} '.format(** point) for point in jsongeo['coord'] ] )
                          
    if jsongeo['geometryType'] == "POINT" :
            return '<Point>\n  <coordinates>{cl}</coordinates>\n </Point>'.format(cl = coordlist)
            
    if jsongeo['geometryType'] == "LINE":
        return (
            '<LineString><coordinates>\n' + coordlist +'\n</coordinates></LineString>'  
        )
    
    
    if jsongeo['geometryType'] == "POLYGON":
        return """
     <Polygon>
      <extrude>1</extrude>
      <altitudeMode>relativeToGround</altitudeMode>
      <outerBoundaryIs>
        <LinearRing>
          <coordinates>
          {cl}
          </coordinates>
        </LinearRing>
      </outerBoundaryIs>
     </Polygon>
      """.format(cl=coordlist)
        
    raise ValueError(jsongeo['geometryType'])
        

def makeColorIconStyles(colors = ['red','green','orange','yellow']):
    res = ""
    for c in colors:
        res += """
        <Style id="placemark-%s">
        <IconStyle>
            <Icon>
            <href>http://maps.me/placemarks/placemark-%s.png</href>
            </Icon>
        </IconStyle>
        </Style>
        """ % (c,c) 
        
    return res


__today = datetime.utcnow()

# start monday next week
__weekstart = __today     - timedelta(days=__today.weekday() - 7)
__weekend   = __weekstart + timedelta(days=7)

__weekstart = int(time.mktime(__weekstart.timetuple()))
__weekend   = int(time.mktime(__weekend.timetuple()))



def makeOperatingTable(id):
    try:
        url = 'https://vaarweginformatie.nl/frp/api/geo/schedule/?id={id}&validFrom={start}000&validUntil={end}000'.format(
            id = id,
            start = __weekstart,
            end = __weekend)
        
        with urllib.request.urlopen(url) as file:
            sched = json.load(file)
    except Exception as e: 
        print("failed to download schedule for {0} ".format(id))
        print(repr(e))
        print(url)
        return ""
    
    res = "<h2> shedule </h2>"
    
    for note in sched.get('periodicNotices', []):
        noteStart = note.get('startDate', '.')
        noteEnd   = note.get('endDate'  , '.')
        msg = note['note']
        res += "<b>note: {noteStart} - {noteEnd} : </b>  {msg}".format(**locals())
    
    events = sched['operatingEvents']
    if len(events) == 0:
        return res
    
    res += '<table>'
    
    def readDateTime(datestr):
        return datetime.fromisoformat(datestr.rstrip('Z'))
    
    # reverse so we can pop from the beginning
    events.reverse()
    
    # guarantee it is in the past for the first time
    
    
    weekdays = [ 'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun' ]
    
    evt = events.pop()
    startTime = readDateTime(evt['start'])
    endTime   = readDateTime(evt['end'])
    
    
    while len(events) > 0:
        dt=startTime.date()
        res += '<tr><th><b>{weekday}:</b>{date}</th><td><ul>'.format(date = dt, weekday=weekdays[dt.weekday()])
        while startTime.date() == dt:
            note = evt.get('recommendation','') + evt.get('note', '-')
            res += '\n<li> <b> {start} - {end}:</b> {note} </li>'.format(
                start = startTime.strftime('%H:%M'),
                end   =   endTime.strftime('%H:%M'),
                note  = note
                )
            
            if not events: 
                break
            evt = events.pop()
            startTime = readDateTime(evt['start'])
            endTime   = readDateTime(evt['end'])
        res += '</ul></td></tr>'
    return res
        
        
        
    
    


